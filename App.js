/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React from 'react';
import { StackNavigator } from 'react-navigation';

import HomeScreen from './HomeScreen'
import PdfView from './PdfView'

const App = StackNavigator({
  HomeScreen: { screen: HomeScreen},
  PdfView: { screen: PdfView},
})

export default App;