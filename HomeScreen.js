

import React, { Component } from 'react';
import { Text, StyleSheet, View, TextInput, Button, Alert } from 'react-native';

const styles = StyleSheet.create({
  container: {
    paddingTop: 100
 },
    urlContainer:{
      fontWeight: 'bold',
      fontSize: 20,
      marginLeft: 10,
    },
    flowRight: {
      flexDirection: 'row',
      alignItems: 'center',
      alignSelf: 'stretch',
    },
    searchInput: {
      height: 36,
      padding: 4,
      marginRight: 30,
      flexGrow: 1,
      fontSize: 20,
      borderWidth: 1,
      borderColor: '#48BBEC',
      borderRadius: 8,
      color: '#48BBEC',
    },
    input: {
      fontWeight: 'bold',
      fontSize: 20,
      margin: 15,
      height: 40,
      borderColor: '#7a42f4',
      borderWidth: 1,
      flex: 1,
   },
})
export default class HomeScreen extends Component{

    static navigationOptions = {
        title: 'Home'
    }

  constructor(props){
    super(props);
    this.state = {
      url: '',
      timeLimit: 0
    }
  }

  saveUrl = (text) => {
    this.setState({url: text})
  }

  saveTimer = (text) => {
    this.setState({timeLimit: text})
  }

  goToPDf = (url, timeLimit) => {
    
  }

    render(){
      const { navigate } = this.props.navigation;
      return(
        <View style = {styles.container}>
        <View style = {styles.flowRight}>
          <Text style = {styles.urlContainer} > Enter Url </Text>
          <TextInput style = {styles.input}
              underlineColorAndroid = "transparent"
              placeholderTextColor = "#9a73ef"
               autoCapitalize = "none"
               onChangeText = {this.saveUrl}
        />
        </View>
        <View style = {styles.flowRight}>
          <Text style = {styles.urlContainer} > Time Limit </Text>
          <TextInput style = {styles.input}
              keyboardType = 'numeric'
              maxLength = {2}
              underlineColorAndroid = "transparent"
              placeholderTextColor = "#9a73ef"
               autoCapitalize = "none"
               onChangeText = {this.saveTimer}
        />
        </View>
        <View style = {{flex:1}} >
          <Button title = 'ViewPdf' 
              onPress={() => navigate("PdfView", {time: this.state.timeLimit})} > 
            </Button>
          </View>
        
        </View>
        )
    }
}

