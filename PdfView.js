import React, { Component } from 'react'
import { StyleSheet, Dimensions, View, Text} from 'react-native'

import Pdf from 'react-native-pdf'
import CountdownCircle from 'react-native-countdown-circle'

const source = {uri:'http://samples.leanpub.com/thereactnativebook-sample.pdf'};

const styles = StyleSheet.create({
    container:{
      flex: 1, 
      justifyContent: 'flex-start',
      alignItems: 'center',
      marginTop: 25,
    },

    pdf:{
      flex: 1,
      width: Dimensions.get('window').width,
    }
});


export default class viewPDF extends React.Component{

  constructor(props) {
    super(props);
    this.state = {
        page: 1,
        pageCount: 1,
        timeElapsed: false,
    };
    this.pdf = null;
}

  render(){
   // const {state} = props.navigation;
   return(
      <View style = {styles.container}>
        {this.state.timeElapsed ? <View style = {styles.container} /> : 
        <Pdf
                     source = {source}
                     onLoadComplete={(numberOfPages,filePath)=>{
                          this.setState({pageCount: pageCount});
                           console.log(`number of pages: ${numberOfPages}`);
                      }}
                      onPageChanged={(page,numberOfPages)=>{
                           this.setState({page:page});
                           console.log(`current page: ${page}`);
                       }}
                      onError={(error)=>{
                            console.log(error);
                       }}
                     style = {styles.pdf}
                    />}
      <CountdownCircle
            seconds={ this.props.navigation.state.params.time }
            radius={30}
            borderWidth={8}
            color="#ff003f"
            bgColor="#fff"
            textStyle={{ fontSize: 20 }}
            onTimeElapsed={() =>{
              this.setState({timeElapsed: true});
              console.log('Elapsed!')
            }}   
        />
        </View>
    )
  }
}